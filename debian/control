Source: libcgi-auth-foaf-ssl-perl
Section: perl
Priority: optional
Build-Depends: devscripts (>= 2.10.7~),
 perl,
 cdbs (>= 0.4.85~),
 debhelper (>= 7.0.1),
 dh-buildinfo,
 libcgi-session-perl,
 libcommon-sense-perl,
 libcrypt-x509-perl (>= 0.50),
 libdatetime-perl,
 libobject-id-perl,
 perl (>= 5.10.1) | libparent-perl,
 libwww-perl,
 librdf-trineshortcuts-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>,
 Florian Schlichting <fschlich@zedat.fu-berlin.de>
Standards-Version: 3.9.2
Vcs-Git: git://git.debian.org/git/pkg-perl/packages/libcgi-auth-foaf-ssl-perl
Vcs-Browser: http://git.debian.org/?p=pkg-perl/packages/libcgi-auth-foaf-ssl-perl.git
Homepage: http://search.cpan.org/dist/CGI-Auth-FOAF_SSL/

Package: libcgi-auth-foaf-ssl-perl
Architecture: all
Depends: ${perl:Depends}, ${misc:Depends}, ${cdbs:Depends}
Recommends: ${cdbs:Recommends}
Description: authentication using WebID
 FOAF+SSL (a.k.a. WebID) is a simple authentication scheme described at
 <http://esw.w3.org/topic/foaf+ssl>. CGI::Auth::FOAF_SSL implements the
 server end of FOAF+SSL in Perl.
 .
 It is suitable for handling authentication using FOAF+SSL over HTTPS.
 Your web server needs to be using HTTPS, configured to request client
 certificates, and make the certificate PEM available to your script.
